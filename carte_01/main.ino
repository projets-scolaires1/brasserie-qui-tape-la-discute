// Carte 01 : include
#include <stdio.h>
#include <string.h>
#include "Ultrasonic.h"
#include "Adafruit_NeoPixel.h"

#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6
// How many NeoPixels are attached to the Arduino
#define NUMPIXELS      10
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
// Carte 01 : pins
#define ULTRASONIC_NC 8
#define ULTRASONIC_SIG 7
#define BLUETOOTH_RXD 14
#define BLUETOOTH_TXD 15
#define TEMP_ANALOG A0

//En temps normal

// #define VANNE_HOUBLON 2
// #define VANNE_MALT 3
// #define VANNE_EAU 4
// #define VANNE_FILTRE 5
// #define VANNE_EGOUT 6
// #define VANNE_STOCKAGE 9
// #define VANNE_FERMENTATION 10
// #define MOTEUR_POMPE 51
// #define MOTEUR_AGITATEUR 49
// #define MOTEUR_FILTRE 47

// Pour la barre de led

#define VANNE_HOUBLON 0
#define VANNE_MALT 1
#define VANNE_EAU 2
#define VANNE_FILTRE 3
#define VANNE_EGOUT 4
#define VANNE_STOCKAGE 5
#define VANNE_FERMENTATION 6
#define MOTEUR_POMPE 7
#define MOTEUR_AGITATEUR 8
#define MOTEUR_FILTRE 9

#define HIGH_COLOR 255
#define MID_COLOR 127
#define LOW_COLOR 0

// Modifier cette constante pour mettre des temps d'attente différents pendant la démo
#define TEMPO 10000

Ultrasonic ultrasonic(ULTRASONIC_SIG);

void setup()
{
    #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) {
        clock_prescale_set(clock_div_1);
    }
    #endif
    // End of trinket special code
    pixels.setBrightness(25);
    pixels.begin(); // This initializes the NeoPixel library.
    // Ouvre la voie série avec l'ordinateur
    Serial.begin(9600);
}

// enum Etats {
//   1,// C1_EMPATAGE
//   2,// C1_FILTRAGE
//   3,// C1_FIN_FILTRAGE
//   4,// C1_EBULLITION
//   5,// C1_TRANSVASAGE
//   6 // C1_NETTOYAGE
// };
unsigned short etatActuel = 1;

enum Etats_Empatage {
  START,
  REMPLISSAGE_EAU,
  CHAUFFAGE,
  AJOUT_MALT,
  CONTROLE_TEMPERATURE,
  TERMINER
};
Etats_Empatage etatActuelEmpatage = START;

enum Etats_Filtrage {
  OUVRIR_VANNES,
  TERMINER_FILTRAGE
};
Etats_Filtrage etatActuelFiltrage = OUVRIR_VANNES;

enum Etats_Fin_Filtrage {
  START_FIN_FILTRAGE,
  FERMER_VANNES,
  TERMINER_FIN_FILTRAGE
};
Etats_Fin_Filtrage etatActuelFinFiltrage = START_FIN_FILTRAGE;

enum Etats_Nettoyage {
  OUVRIR_VANNES_NETTOYAGE,
  RETOURNER_FILTRE,
  VERSER_EAU,
  TERMINER_NETTOYAGE
};
Etats_Nettoyage etatActuelNettoyage = OUVRIR_VANNES_NETTOYAGE;

enum Etats_Ebullition {
  START_EBULLITION,
  POMPER_EAU,
  AJOUTER_HOUBLON,
  VERIFIER_TEMPERATURE,
  TERMINER_EBULLITION
};
Etats_Ebullition etatActuelEbullition = START_EBULLITION;

enum Etats_Transvasage {
  OUVRIR_VANNES_TRANSVASAGE,
  TRANSVASER_CONTENU,
  FERMER_VANNES_TRANSVASAGE,
  TERMINER_TRANSVASAGE
};
Etats_Transvasage etatActuelTransvasage = OUVRIR_VANNES_TRANSVASAGE;

const unsigned short setTemperature = 72;
const unsigned short distanceEauCapteur = 5;
unsigned long startTime;
int temperature;
unsigned short numeroNettoyage = 0;
long RangeInCentimeters;

/**
 * Selon le message reçu par le bluetooth, envoie vers les différents processus du brassage
 */
void loop() // run over and over
{
   switch (etatActuel) {
      case 1:
        Empatage();
        break;

      case 2:
        Filtrage();
        break;

      case 3:
        FinFiltrage();
        break;

      case 4:
        Ebullition();
        break;

      case 5:
        Transvasage();
        break;

      case 6:
        Nettoyage();
        break;

      default:
        break;
   }
}

void Empatage()
{
  switch (etatActuelEmpatage) {
    case START:
      startTime = millis();
      etatActuelEmpatage = REMPLISSAGE_EAU;
      break;

    case REMPLISSAGE_EAU:
      //ON VANNE_EAU
      writeLED(VANNE_EAU, HIGH_COLOR, LOW_COLOR, LOW_COLOR);
      // Savoir s'il y a assez d'eau dans la cuve
      RangeInCentimeters = ultrasonic.MeasureInCentimeters();
      if (RangeInCentimeters < 7) { // 7 cm
        //OFF VANNE_EAU
        writeLED(VANNE_EAU, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        startTime = millis();
        etatActuelEmpatage = CHAUFFAGE;
      }
      break;

    case CHAUFFAGE:
      // TODO : Chauffer jusqu'à 72 deg (Pas avec un timer quoi)
      if (millis() - startTime >= TEMPO) {
        startTime = millis();
        etatActuelEmpatage = AJOUT_MALT;
      }
      break;

    case AJOUT_MALT:
      //ON VANNE_MALT
      writeLED(VANNE_MALT, LOW_COLOR, HIGH_COLOR, LOW_COLOR);
      // TODO : Tant qu'on veut du malt (Pas avec un timer quoi)
      if (millis() - startTime >= TEMPO) {
        //OFF VANNE_MALT
        writeLED(VANNE_MALT, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        startTime = millis();
        etatActuelEmpatage = CONTROLE_TEMPERATURE;
      }
      break;

    case CONTROLE_TEMPERATURE:
      // TODO : Chauffer 1 heure si on descend en dessous de 65 deg rechauffer
      //ON MOTEUR_AGITATEUR
      writeLED(MOTEUR_AGITATEUR, MID_COLOR, MID_COLOR, MID_COLOR);
      if (millis() - startTime >= TEMPO) {
        //OFF MOTEUR_AGITATEUR
        writeLED(MOTEUR_AGITATEUR, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        etatActuelEmpatage = TERMINER;
      }
      break;

    case TERMINER:
      etatActuelEmpatage = START;
      etatActuel = 2;
      break;

    default:
      break;
  }
}

void Filtrage()
{
  switch (etatActuelFiltrage) {

    case OUVRIR_VANNES:
      // Vérifier que les vannes fermentation et egout sont bien fermées
      //OFF VANNE_FERMENTATION
      writeLED(VANNE_FERMENTATION, LOW_COLOR, LOW_COLOR, LOW_COLOR);
      //OFF VANNE_EGOUT
      writeLED(VANNE_EGOUT, LOW_COLOR, LOW_COLOR, LOW_COLOR);
      //ON VANNE_FILTRE
      writeLED(VANNE_FILTRE, LOW_COLOR, LOW_COLOR, HIGH_COLOR);
      //ON VANNE_STOCKAGE
      writeLED(VANNE_STOCKAGE, HIGH_COLOR, MID_COLOR, MID_COLOR);
      etatActuelFiltrage = TERMINER_FILTRAGE;
      break;

    case TERMINER_FILTRAGE:
      etatActuelFiltrage = OUVRIR_VANNES;
      etatActuel = 3;
      break;

    default:
      break;
  }
}

void FinFiltrage()
{
  switch (etatActuelFinFiltrage) {
    case START_FIN_FILTRAGE:
      startTime = millis();
      etatActuelFinFiltrage = FERMER_VANNES;
      break;

    case FERMER_VANNES:
      // TODO : Attendre de recevoir l'infos comme quoi toute l'eau a été pompé vers la cuve de stockage
      if (millis() - startTime >= TEMPO) {
        //OFF VANNE_FILTRE
        writeLED(VANNE_FILTRE, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        //OFF VANNE_STOCKAGE
        writeLED(VANNE_STOCKAGE, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        etatActuelFinFiltrage = TERMINER_FIN_FILTRAGE;
      }
      break;

    case TERMINER_FIN_FILTRAGE:
      etatActuelFinFiltrage = START_FIN_FILTRAGE;
      etatActuel = 6;
      break;

    default:
      break;
  }
}

void Nettoyage()
{
  switch (etatActuelNettoyage) {

    case OUVRIR_VANNES_NETTOYAGE:
      //ON VANNE_EGOUT
      writeLED(VANNE_EGOUT, MID_COLOR, MID_COLOR, HIGH_COLOR);
      //ON VANNE_FILTRE
      writeLED(VANNE_FILTRE, LOW_COLOR, LOW_COLOR, HIGH_COLOR);
      startTime = millis();
      etatActuelNettoyage = RETOURNER_FILTRE;
      break;

    case RETOURNER_FILTRE:
      //ON MOTEUR_FILTRE
      writeLED(MOTEUR_FILTRE, HIGH_COLOR, MID_COLOR, LOW_COLOR);
      // TODO : Faire tourner le filtre à 180° (pas avec un timer quoi)
      if (millis() - startTime >= TEMPO) {
        //ON MOTEUR_FILTRE
        writeLED(MOTEUR_FILTRE, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        startTime = millis();
        etatActuelNettoyage = VERSER_EAU;
      }
      break;

    case VERSER_EAU:
      //ON VANNE_EAU
      writeLED(VANNE_EAU, HIGH_COLOR, LOW_COLOR, LOW_COLOR);
      // Attendre que le filtre soit nettoyé
      if (millis() - startTime >= TEMPO) {
        //OFF VANNE_EAU
        writeLED(VANNE_EAU, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        //OFF VANNE_FILTRE
        writeLED(VANNE_FILTRE, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        //OFF VANNE_STOCKAGE
        writeLED(VANNE_EGOUT, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        etatActuelNettoyage = TERMINER_NETTOYAGE;
      }
      break;

    case TERMINER_NETTOYAGE:
      etatActuelNettoyage = OUVRIR_VANNES_NETTOYAGE;
      if(numeroNettoyage == 0)
      {
        numeroNettoyage = 1;
        etatActuel = 4;
      }
      else
      {
        numeroNettoyage = 0;
        etatActuel = 1;
      }
      break;

    default:
      break;
  }
}

void Ebullition()
{
  switch (etatActuelEbullition) {
    /***************************************************** Pré-ébullition  *****************************************************/
    case START_EBULLITION:
      //ON MOTEUR_POMPE
      writeLED(MOTEUR_POMPE, MID_COLOR, HIGH_COLOR, LOW_COLOR);
      startTime = millis();
      etatActuelEbullition = POMPER_EAU;
      break;

    case POMPER_EAU:
      // TODO : Arrêter lorsqu'il n'y a plus d'eau (Pas avec un timer quoi)
      if (millis() - startTime >= TEMPO) {
        //OFF MOTEUR_POMPE
        writeLED(MOTEUR_POMPE, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        //ON VANNE_HOUBLON
        writeLED(VANNE_HOUBLON, MID_COLOR, LOW_COLOR, HIGH_COLOR);
        startTime = millis();
        etatActuelEbullition = AJOUTER_HOUBLON;
      }
      break;

    /*****************************************************    Ebullition   *****************************************************/
    case AJOUTER_HOUBLON:
      // TODO : Faire en sorte de savoir quand arrêter de mettre du houblon (Pas avec un timer quoi)
      if (millis() - startTime >= TEMPO) {
        //OFF VANNE_HOUBLON
        writeLED(VANNE_HOUBLON, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        startTime = millis();
        etatActuelEbullition = VERIFIER_TEMPERATURE;
      }
      break;

    case VERIFIER_TEMPERATURE:
      // TODO : Chauffer constamment pour maintenir l'ébullition pdt 1h
      //ON MOTEUR_AGITATEUR
      writeLED(MOTEUR_AGITATEUR, MID_COLOR, MID_COLOR, MID_COLOR);
      if (millis() - startTime >= TEMPO) {
        //OFF MOTEUR_AGITATEUR
        writeLED(MOTEUR_AGITATEUR, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        etatActuelEbullition = TERMINER_EBULLITION;
      }
      break;

    case TERMINER_EBULLITION:
      etatActuelEbullition = START_EBULLITION;
      etatActuel = 5;
      break;

    default:
      break;
  }
}

void Transvasage()
{
  switch (etatActuelTransvasage) {
    case OUVRIR_VANNES_TRANSVASAGE:
      //ON VANNE_FERMENTATION
      writeLED(VANNE_FERMENTATION, MID_COLOR, HIGH_COLOR, MID_COLOR);
      //ON VANNE_FILTRE
      writeLED(VANNE_FILTRE, LOW_COLOR, LOW_COLOR, HIGH_COLOR);
      startTime = millis();
      etatActuelTransvasage = TRANSVASER_CONTENU;
      break;

    case TRANSVASER_CONTENU:
      // Transvaser le contenu de la cuve vers la fermentation
      if (millis() - startTime >= TEMPO) {
        // TODO : Arrêter lorsqu'il n'y a plus d'eau (Pas avec un timer quoi)
        //OFF VANNE_FERMENTATION
        writeLED(VANNE_FERMENTATION, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        //OFF VANNE_FILTRE
        writeLED(VANNE_FILTRE, LOW_COLOR, LOW_COLOR, LOW_COLOR);
        startTime = millis();
        etatActuelTransvasage = FERMER_VANNES_TRANSVASAGE;
      }
      break;

    case FERMER_VANNES_TRANSVASAGE:
      // TODO : Vérifier que tout le liquide est dans la cuve de fermentation
      etatActuelTransvasage = TERMINER_TRANSVASAGE;
      break;

    case TERMINER_TRANSVASAGE:
      etatActuelTransvasage = OUVRIR_VANNES_TRANSVASAGE;
      etatActuel = 6;
      break;

    default:
      break;
  }
}

void writeLED(unsigned short pin, unsigned short R, unsigned short G, unsigned short B){
  pixels.setPixelColor(pin, pixels.Color(R, G, B));
  pixels.show();
}
