#include <SPI.h>

#include <SoftwareSerial.h>
SoftwareSerial BLT(8, 9); //(RX, TX)

#include <string.h>
#include <OneWire.h>

/* #############################
  #       POUR LA COM BLT      #
  ##############################
 */
/* peut etre pas besoin de rien */
enum ETAT_FLUX_BLT {
  EN_COURS,
  FINI,
  RIEN
};
ETAT_FLUX_BLT etat_emission = RIEN;
ETAT_FLUX_BLT etat_reception = FINI;

String message_emission = "";
String message_reception = "";

enum ETAT_MESSAGE {
  ATraiter,
  Traite
};
ETAT_MESSAGE etat_message_emission = Traite;
ETAT_MESSAGE etat_message_reception = Traite;
//Pour enregistrer les coms
unsigned char envoyeur;
unsigned char receveur;
unsigned char commande;
char* parametre;
unsigned char capteur;
int donnee;
//############################################

const int led_levure = 7;
const int DS18S20_Pin = 2; //DS18S20 Signal pin on digital 2
//Temperature chip i/o
OneWire ds(DS18S20_Pin);  // on digital pin 2

//moteur
const int enableBridge = 12;
const int MotorForward = 10;
const int MotorReverse = 11;
int Power = 200; //Motor velocity between 0 and 255

int timeStartLevure = -1;
int timeStartMoteur = -1;

unsigned char etatApplication = 0;
const unsigned char etatErreur = 100;

int temp_palier = 24;

void setup() {
  // Réglage du port numérique associé à la LED : mode sortie = OUTPUT
  pinMode(led_levure, OUTPUT);
  digitalWrite(led_levure, LOW);

  // moteur
  pinMode(MotorForward,OUTPUT);
  pinMode(MotorReverse,OUTPUT);
  pinMode(enableBridge,OUTPUT);
  
  Serial.begin(9600);
  BLT.begin(9600);
}

void loop() {
  //recevoir une commande
  Reception();
  Analyse();
  Emission();

  switch (commande) {
    //commande 1 : demande de fermentation
    case 1:
      C3_FERMENTATION();
      commande = 0;
      break;

    //commande 2 : demande envoi de l'état courrant de la carte
    case 2:
      C3_ENVOIE_ETAT();
      commande = 0;
      break;

    //commande 3 : changement de la température palier
    case 3:
      int nouveau_palier = atoi(parametre);
      //TODO NOMECLATURE DU PARAM
      C3_CHGMNT_TEMP_PALER(nouveau_palier);
      commande = 0;
      break;
  }

  switch (etatApplication) {
    // Verfication de la température du liquide jusqu'à ce qu'elle atteigne ou soit en dessous
    // de la température palier
    case 1:
      checkTemp();
      break;

    // La température de palier a été atteinte on verse la levure
    // on considère que en 5 secondes la dose de levure a été versée
    case 2:
     if (timeStartLevure == -1) {
       // on démarre le temps de départ le versage
       timeStartLevure = millis();
       verser_levure(1);
     }
     // 5 secondes plus tard
     if ( (millis() - timeStartLevure) >= 5000 ){
       verser_levure(0);
       timeStartLevure = -1;
       etatApplication = 3;
     }
     break;

    // La levure a fini d'être verser, on mélange pendant 5 secondes
    case 3:
     if (timeStartMoteur == -1) {
       // on démarre le temps de départ du moteur
       timeStartMoteur = millis();
       melanger(1);
     }
     // 5 secondes plus tard
     if ( (millis() - timeStartMoteur) >= 5000 ){
       melanger(0);
       timeStartMoteur = -1;
     }
      break;
    
    // Une erreur a eu lieu, on bloque tout
    case etatErreur:
      Serial.println("En erreur !");
      analogWrite(MotorForward,0);
      analogWrite(MotorReverse,0);
      digitalWrite(enableBridge,LOW);
      digitalWrite(led_levure, LOW);
      break;
      
    default:
      break;
  }
}

void C3_FERMENTATION() {
  //demande de fermentation d'un liquide, on passe la carte dans l'etat 1, pour vérifier
  //la température du liquide
  etatApplication = 1;
}

void C3_CHGMNT_TEMP_PALER(int temp){
  Serial.println("Chgm palier : " + temp);
  temp_palier = temp;
}

void C3_ENVOIE_ETAT() {
  //TODO envoyer chaine
  char chaine[100] = "";
  strcpy(chaine,"température du mélange : ");
  char buf[5];
  snprintf(buf, 4, "%f", getTemp());
  strcat(chaine, buf);
  strcat(chaine, " ; ");
  
  switch (etatApplication) {
    case 1:
      strcat(chaine,"en attente de l'abaissement de la température du liquide");
      break;
      
    case 2:
      strcat(chaine,"versage de la levure en cours");
      break;
      
    case 3:
      strcat(chaine,"mélangeage du liquide en cours");
      break;
      
    case etatErreur:
      strcat(chaine,"carte en erreur !");
      break;
      
    default:
      strcat(chaine,"carte en attente, aucune action en cours");
      break;
  }
}

void checkTemp() {
  float actual_temp = getTemp();
  if (actual_temp <= temp_palier) {
    // la température palier est atteinte on change d'état dans l'application
    Serial.println("Temp palier atteinte");
    etatApplication = 2;
  }
}

void verser_levure(unsigned char state) {
  //state = 1 on verse
  //state = 0 on arrete de verser
  if (state == 1){
    digitalWrite(led_levure, HIGH);
  }
  if (state == 0){
    digitalWrite(led_levure, LOW);
    //On change l'état de l'application vers 3 on va mélanger
  }
}

void melanger(unsigned char state) {
  //state = 1 on mélange
  //state = 0 on arrete de mélanger
  if (state == 1){
    Serial.println("Début mélange");
    digitalWrite(enableBridge,HIGH);
    analogWrite(MotorForward,Power);
    analogWrite(MotorReverse,0);
  }
  if (state == 0){
    Serial.println("Fin mélange");
    analogWrite(MotorForward,0);
    analogWrite(MotorReverse,0);
    digitalWrite(enableBridge,LOW);
    //On change l'état de l'application vers 0 on a fini le traitement
    etatApplication = 0;
  }
}

float getTemp(){
  //returns the temperature from one DS18S20 in DEG Celsius

  byte data[12];
  byte addr[8];

  if ( !ds.search(addr)) {
      //no more sensors on chain, reset search
      ds.reset_search();
      return -1000;
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return -1000;
  }

  if ( addr[0] != 0x10 && addr[0] != 0x28) {
      Serial.print("Device is not recognized");
      return -1000;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end

  byte present = ds.reset();
  ds.select(addr);
  ds.write(0xBE); // Read Scratchpad


  for (int i = 0; i < 9; i++) { // we need 9 bytes
    data[i] = ds.read();
  }

  ds.reset_search();

  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TemperatureSum = tempRead / 16;

  return TemperatureSum;
}
