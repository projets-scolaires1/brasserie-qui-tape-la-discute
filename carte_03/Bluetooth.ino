void Reception() {
  char data = ' ';
  if (BLT.available()) {
    etat_reception = EN_COURS;
    data = BLT.read();
    if (data == '\n') {
      etat_reception = FINI;
      etat_message_reception = ATraiter;
    } else {
      message_reception += data;
    }
  }
}

//erreur if message haven't good format
void Analyse() {
  if (etat_reception == FINI && message_reception.compareTo("") != 0 && etat_message_reception == ATraiter) {
    //mettre autre analyse
    Serial.print("Le message : ");
    Serial.println(message_reception);

    /* split and save */
    char str[50];
    message_reception.toCharArray(str, 50);
     
    char *split;
    split = strtok(str, ":");
    envoyeur = atoi(split);
    Serial.print("envoyeur : ");
    Serial.print(envoyeur);
    Serial.print("| ");

    split = strtok(NULL, ":");
    receveur = atoi(split);
    Serial.print("receveur : ");
    Serial.print(receveur);
    Serial.print("| ");

    split = strtok(NULL, ":");

    if (atoi(split) == 0 ) {
      split = strtok(NULL, ":");
      commande = atoi(split);
      Serial.print("commande : ");
      Serial.print(commande);
      Serial.print("| ");

      split = strtok(NULL, ":");
      parametre = split;
      Serial.print("parametre : ");
      Serial.print(parametre);
      Serial.print("|\n");
    } else {
      split = strtok(NULL, ":");
      capteur = atoi(split);
      Serial.print("capteur : ");
      Serial.print(capteur);
      Serial.print("| ");

      split = strtok(NULL, ":");
      donnee = atoi(split);
      Serial.print("donnee : ");
      Serial.print(donnee);
      Serial.print("|\n");
    }
    etat_message_reception = Traite;
    message_reception = "";
  }
}

void Emission() {
  switch (etat_emission) {
    case EN_COURS:
      BLT.print(message_emission);
      Serial.println("ENVOIE : " + message_emission);
      etat_emission = FINI;
      break;
    default:
      break;
  }
}