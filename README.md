# Brasserie Qui Tape La Discute

> Equipe :
>
> - Marjorie Latger
> - Mathis Schmidt
> - Lucas Lagrèze
> - Quentin Lemesle
> - Romain Le Rest
> - Steven Gourves

## Description

Simulation d'une brasserie connectée en arduino (carte arduino uno et due).

![Schéma de la brasserie](./docs/schema_brasserie.jpg)

## Specifications techniques

Numéro des cartes :

```c
#define CARTE_01 1
#define CARTE_02 2
#define CARTE_03 3
#define CARTE_PATRONNE 4
```

### Capteurs des cartes

#### Carte 1

- 1 bluetooth
- 1 capteur de température
- 1 capteur de pression
- 1 capteur de ultrasons
- 7 electro-vannes (remplacées par des moteurs)
- 1 moteur (pour mélanger)
- 1 moteur (pour tourner le filtre)
- 1 pompe

#### Carte 2

- 1 bluetooth
- 1 electro-vanne
- 1 pompe

#### Carte 3

- 1 bluetooth
- 1 capteur d'alcool
- 1 capteur de densité

#### Carte patronne

- 1 bluetooth
- 1 lorawan

### Orchestration & Commandes

#### Orchestration

Toutes les étapes de la brasserie sont orchestrées par la carte patronne, tel que :

1. Empatage

    **Carte 1** : *C1_EMPATAGE*

    - Faire tomber l'eau dans la cuve (C1E)
    - Faire chauffer l'eau 72°C
    - Faire tomber le malt dans la cuve (C1M)
    - Pendant 1h :

        - Contrôler la température
        - Contrôler la pression

2. Filtrage et sauvegarde du liquide

    **Carte 1** : *C1_FILTRAGE*

    - Fermer la vanne C3F
    - Fermer la vanne C1P
    - Ouvrir la cuve (filtre) C1F
    - Ouvrir la vanne vers le stockage C1S

    **Carte 2** : *C2_RECUPERATION_LIQUIDE_CUVE*

    - Fermer la vanne C2S
    - Pomper vers la cuve de stockage

    **Carte 1** : *C1_FIN_FILTRAGE*

    - Fermer la cuve C1S
    - Fermer la cuve (filtre) C1F

3. Nettoyage de la cuve

    **Carte 1** : *C1_NETTOYAGE*

    - Ouvrir la vanne C1P
    - Ouvrir la vanne C1F
    - Retourner le filtre
    - Faire tomber l'eau dans la cuve (C1E)
    - Fermer la vanne C1P
    - Fermer la vanne C1F

4. Pré-ebullition

    **Carte 2** : *C2_OUVRIR_VANNE*

    - Ouvrir la vanne de stockage C2S

    **Carte 1** : *C1_PRE_EBULLITION*

    - Pomper vers la cuve principale

5. Ebullition

    **Carte 1** : *C1_EBULLITION*

    - Faire tomber le houblon dans la cuve (C1H)
    - Monter la température à 100°C
    - Attendre 1h

6. Transvasage

    **Carte 1** : *C1_TRANSVASAGE*

    - Ouvrir la vanne C3F
    - Ouvrir la vanne C1F
    - Fermer la vanne C1F
    - Fermer la vanne C3F

7. Nettoyage de la cuve

    **Carte 1** : *C1_NETTOYAGE*

    - Ouvrir la vanne C1P
    - Ouvrir la vanne C1F
    - Retourner le filtre
    - Faire tomber l'eau dans la cuve (C1E)
    - Fermer la vanne C1P
    - Fermer la vanne C1F

8. Fermentation

    **Carte 3** : *C3_FERMENTATION*

    - Attendre température inférieure ou égale à 24°C
    - Faire tomber le levure dans la cuve (C3L)
    - Mélanger pendant X secondes
    - Attendre 3 semaines

#### Commandes

Ce sont les commandes envoyées par l'utilisateur à la carte patronne.

1. Lancement du brassage (Ajouter les paramètres température de X°C, pendant X heure)

    **Carte patronne** : *C4_LANCEMENT_BRASSAGE*

    - Initialisation de `etapeEnCours` à 1
    - Initialisation de `etat` à 0

2. Arrêt du brassage

    **Carte patronne** : *C4_ARRET_BRASSAGE*

    - Initialisation de `etapeEnCours` à 0
    - Initialisation de `etat` à 0

3. Demander l'état de brassage

    **Carte patronne** : *C4_DEMANDE_ETAT_BRASSAGE*

    - Retourner etapeEnCours et etat

4. Demander la densité de la bière lors de la fermentation

    **Carte patronne** : *C4_DEMANDE_DENSITE*

    - Envoi de la commande `C3_DEMANDE_DENSITE` à la carte 3
    - Retourner la densité

    **Carte 3** : *C3_DEMANDE_DENSITE*

    - Récupérer la densité
    - Retourner la densité

Commandes pour la simulation (car on ne peut pas tester le brassage en vrai).

1. Modification de la température de la cuve

    **Carte patronne** : *C4_MODIFICATION_TEMPERATURE*

    - Envoi de la commande `C1_MODIFICATION_TEMPERATURE` à la carte 1

    **Carte 1** : *C1_MODIFICATION_TEMPERATURE*

    - Modifier la température de la cuve

2. Modification de la pression dans la cuve

    **Carte patronne** : *C4_MODIFICATION_PRESSION*

    - Envoi de la commande `C1_MODIFICATION_PRESSION` à la carte 1

    **Carte 1** : *C1_MODIFICATION_PRESSION*

    - Modifier la pression dans la cuve

3. Modification de la quantité de liquide dans la cuve

    **Carte patronne** : *C4_MODIFICATION_QUANTITE_LIQUIDE*

    - Envoi de la commande `C1_MODIFICATION_QUANTITE_LIQUIDE` à la carte 1

    **Carte 1** : *C1_MODIFICATION_QUANTITE_LIQUIDE*

    - Modifier la quantité de liquide dans la cuve

#### Numéro des commandes

```c
// Carte 01
#define C1_EMPATAGE 1
#define C1_FILTRAGE 2
#define C1_FIN_FILTRAGE 3
#define C1_PRE_EBULLITION 4
#define C1_EBULLITION 5
#define C1_TRANSVASAGE 6
#define C1_NETTOYAGE 7
#define C1_MODIFICATION_TEMPERATURE 8
#define C1_MODIFICATION_PRESSION 9
#define C1_MODIFICATION_QUANTITE_LIQUIDE 10
// Carte 02
#define C2_RECUPERATION_LIQUIDE_CUVE 1
#define C2_OUVRIR_VANNE 2
// Carte 03
#define C3_FERMENTATION 1
#define C3_DEMANDE_DENSITE 2
// Carte 04
#define C4_LANCEMENT_BRASSAGE 1
#define C4_ARRET_BRASSAGE 2
#define C4_DEMANDE_ETAT_BRASSAGE 3
#define C4_DEMANDE_DENSITE 4
#define C4_MODIFICATION_TEMPERATURE 5
#define C4_MODIFICATION_PRESSION 6
#define C4_MODIFICATION_QUANTITE_LIQUIDE 7
```

### Communication

TODO
