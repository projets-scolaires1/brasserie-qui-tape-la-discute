/**
 * @file main.ino
 * @author
 * @brief Gestion de la carte patronne
 * @details ...
 * @version 1.0
 * @date 2023-01-27
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "Arduino.h"
#include "libs/constantes.h"

/**
 * VARIABLES GLOBALES & CONSTANTES
 */
// Messages Bluetooth
char messageRecu[255];
unsigned char envoyeur;
unsigned char receveur;
unsigned char commande;
char *parametre;
unsigned char capteur;
char *donnee;
// Etapes de brassage
int[] ETAPE_DE_BRASSAGE = [
    EMPATAGE,
    FILTRAGE_01,
    FILTRAGE_02,
    FILTRAGE_03,
    NETTOYAGE,
    PRE_EBULLITION_01,
    PRE_EBULLITION_02,
    EBULLITION,
    TRANSVASAGE,
    NETTOYAGE,
    FERMENTATION,
    FINITO_LE_SPAGITO
];
int etapeEnCours = FINITO_LE_SPAGITO;
int etat = ETAT_FAIT;

/**
 * @brief Initialisation de l'application.
 *
 */
void setup() {}

/**
 * @brief Boucle principale de l'application.
 *
 */
void loop()
{
    if (etat != ETAT_ERREUR)
    {
        receptionDonnees();

        etatEtapeDeBrassage();
        if (etat == ETAT_FAIT)
        {
            etapeDeBrassage();
        }

        commandes();
    }
    else
    {
        messageDErreur(); // Allume une led rouge
    }
}
