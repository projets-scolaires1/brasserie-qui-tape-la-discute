/**
 * @file brassage.ino
 * @author
 * @brief Commandes de l'orchestrateur
 * @version 1.0
 * @date 2023-01-27
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "constantes.h"

/**
 * @brief Etapes de brassage
 *
 */
void etapeDeBrassage()
{
    switch (ETAPE_DE_BRASSAGE[etapeEnCours])
    {
    case FINITO_LE_SPAGITO:
        // TODO
        break;
    case EMPATAGE:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_EMPATAGE); // TODO: Ajouter les paramètres (température de 72°C, pendant 1 heure)
        break;
    case FILTRAGE_01:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_FILTRAGE);
        break;
    case FILTRAGE_02:
        envoieOrdre(CARTE_PATRONNE, CARTE_02, TYPE_CMD, C2_RECUPERATION_LIQUIDE_CUVE);
        break;
    case FILTRAGE_03:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_FIN_FILTRAGE);
        break;
    case NETTOYAGE:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_NETTOYAGE);
        break;
    case PRE_EBULLITION_01:
        envoieOrdre(CARTE_PATRONNE, CARTE_02, TYPE_CMD, C2_OUVRIR_VANNE);
        break;
    case PRE_EBULLITION_02:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_PRE_EBULLITION);
        break;
    case EBULLITION:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_EBULLITION);
        break;
    case TRANSVASAGE:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_TRANSVASAGE);
        break;
    case FERMENTATION:
        envoieOrdre(CARTE_PATRONNE, CARTE_03, TYPE_CMD, C3_FERMENTATION);
        break;
    default:
        break;
    }
}

/**
 * @brief Etats de l'étape de brassage
 *
 * Lorsque commande vaut 255, on est en train de recevoir un message d'une carte pour indiquer la fin d'une tâche.
 * Si donnee vaut 1, alors la tâche est terminée avec succès. Sinon, il y a eu une erreur.
 */
void etatEtapeDeBrassage()
{
    if (commande == 255)
    {
        if (donnee == "1")
        {
            etat = ETAT_FAIT;
            etapeEnCours++;
        }
        else
        {
            etat = ETAT_ERREUR;
        }
    }
}
