/**
 * @file brassage.ino
 * @author 
 * @brief Fichier de Mathis pour sa magie de la communication
 * @version 1.0
 * @date 2023-01-27
 * 
 * @copyright Copyright (c) 2023
 * 
 */

void receptionDonnees()
{
    // TODO : Réceptionner les données (A l'aide Mathis !!!)
    // TODO : Traiter les données (A l'aide Mathis !!!)
}

void envoieOrdre(int carteEmettrice, int carteDestinataire, int type, int commande)
{
    // TODO : Envoyer l'ordre à la carte destinataire (A l'aide Mathis !!!)
}

void reponseALUtilisateur(char* reponse)
{
    // TODO : Envoyer la réponse à l'utilisateur (A l'aide Mathis !!!)
}
