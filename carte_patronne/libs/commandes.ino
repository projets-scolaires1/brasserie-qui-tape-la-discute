/**
 * @file commandes.ino
 * @author
 * @brief Commandes envoyées par l'utilisateur
 * @version 1.0
 * @date 2023-01-27
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "Arduino.h"
#include "constantes.h"

/**
 * @brief Différentes commandes envoyées par l'utilisateur
 *
 */
void commande()
{
    switch (commande)
    {
    case C4_LANCEMENT_BRASSAGE:
        etapeEnCours = EMPATAGE;
        etat = ETAT_FAIT;
        break;

    case C4_ARRET_BRASSAGE:
        etapeEnCours = FINITO_LE_SPAGITO;
        etat = ETAT_FAIT;
        break;

    case C4_DEMANDE_ETAT_BRASSAGE:
        reponseALUtilisateur(etapeEnCours + ":" + etat);
        break;

    case C4_DEMANDE_DENSITE:
        envoieOrdre(CARTE_PATRONNE, CARTE_03, TYPE_CMD, C3_DEMANDE_DENSITE);
        // TODO : Envoyer la réponse à l'utilisateur
        break;

    case C4_MODIFICATION_TEMPERATURE:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_MODIFICATION_TEMPERATURE, parametre[0]);
        // TODO : Envoyer la réponse à l'utilisateur
        break;

    case C4_MODIFICATION_PRESSION:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_MODIFICATION_PRESSION, parametre[0]);
        // TODO : Envoyer la réponse à l'utilisateur
        break;

    case C4_MODIFICATION_QUANTITE_LIQUIDE:
        envoieOrdre(CARTE_PATRONNE, CARTE_01, TYPE_CMD, C1_MODIFICATION_QUANTITE_LIQUIDE, parametre[0]);
        // TODO : Envoyer la réponse à l'utilisateur
        break;

    default:
        reponseALUtilisateur("Commande inconnue.");
        break;
    }

    receptionDesReponsesDeModifications();
}

/**
 * @brief Réception des réponses des cartes (des commandes de modifications)
 * 
 */
void receptionDesReponsesDeModifications()
{
    if (envoyeur == CARTE_01 && capteur == C1_CAPTEUR_DENSITE)
    {
        reponseALUtilisateur("Densité : " + donnee);
    }
    else if (envoyeur == CARTE_01 && capteur == C1_CAPTEUR_TEMPERATURE)
    {
        reponseALUtilisateur("Température : " + donnee);
    }
    else if (envoyeur == CARTE_01 && capteur == C1_CAPTEUR_PRESSION)
    {
        reponseALUtilisateur("Pression : " + donnee);
    }
    else if (envoyeur == CARTE_01 && capteur == C1_CAPTEUR_QUANTITE_LIQUIDE)
    {
        reponseALUtilisateur("Quantité liquide : " + donnee);
    }
}
