/*
tx -> rx3
rx -> tx3
mdp: 1234

*/
#include <SoftwareSerial.h>
SoftwareSerial BLT(12, 11); //(RX, TX)
#include <string.h>

/* #############################
  #       POUR LA COM BLT      #
  ##############################
 */
/* peut etre pas besoin de rien */
enum ETAT_FLUX_BLT {
  EN_COURS,
  FINI,
  RIEN
};
ETAT_FLUX_BLT etat_emission = RIEN;
ETAT_FLUX_BLT etat_reception = FINI;

String message_emission = "";
String message_reception = "";

enum ETAT_MESSAGE {
  ATraiter,
  Traite
};
ETAT_MESSAGE etat_message_emission = Traite;
ETAT_MESSAGE etat_message_reception = Traite;
//Pour enregistrer les coms
unsigned char envoyeur;
unsigned char receveur;
unsigned char commande;
char* parametre;
unsigned char capteur;
int donnee;
//############################################

int vanne = 8;
int pompe = 7;

int timeStartPompage = -1;
int timeStartVanne = -1;
unsigned char etatApplication = 0;
const unsigned char etatErreur = 100;

//moteur
const int enableBridge1 = 2;
const int MotorForward1 = 3;
const int MotorReverse1 = 5;
int Power = 200; //Motor velocity between 0 and 255

void setup() {
  // Réglage du port numérique associé à la LED : mode sortie = OUTPUT
  pinMode(vanne, OUTPUT);
  pinMode(pompe, OUTPUT);
  digitalWrite(vanne, LOW);
  digitalWrite(pompe, LOW);

  // moteur
  pinMode(MotorForward1,OUTPUT);
  pinMode(MotorReverse1,OUTPUT);
  pinMode(enableBridge1,OUTPUT);
  
  Serial.begin(9600);
  BLT.begin(9600);
}

/*
 * Cette carte pompe le liquide dans la cuve de stockage sur demande
 * Elle peut également ouvrir et fermer la vanne de sortie de la cuve de stockage
 */

void loop() {  
  //recevoir une commande
  Reception();
  Analyse();
  Emission();
  
  switch (commande) {
    //commande 1 : demande de pompage du liquide vers la cuve de stockage
    case 1:
      C2_RECUPERATION_LIQUIDE_CUVE();
      commande = 0;
      Serial.println("C2_RECUPERATION_LIQUIDE_CUVE");
      break;
    //commande 2 : demande d'ouverture de la vanne de sortie de la cuve de stockage
    case 2:
      C2_OUVRIR_VANNE();
      commande = 0;
      Serial.println("C2_OUVRIR_VANNE");
      break;
    //commande 3 : demande d'envoi de l'état courrant de la carte
    case 3:
      C2_ENVOIE_ETAT();
      Serial.println("C2_ENVOIE_ETAT");
      commande = 0;
      break;
  }

  switch (etatApplication) {
    // Fermeture vanne de sortie de cuve
    case 1:
      if (timeStartVanne == -1) {
        // on démarre le temps de départ pour la vanne
        timeStartVanne = millis();
      }
      gestionVanne(1,0);
      // Simulation fermeture dure 3 secondes
      if ( (millis() - timeStartVanne) >= 3000 ){
        finVanne();
      }
      break;
    case 2:
      if (timeStartPompage == -1) {
        // on démarre le temps de départ pour le pompage
        timeStartPompage = millis();
      }
      gestionPompe(1);
      // Simulation pompage dure 5 secondes
      if ( (millis() - timeStartPompage) >= 5000 ){
        finPompe();
      }
      break;
    case 3:
      if (timeStartVanne == -1) {
        // on démarre le temps de départ pour la vanne
        timeStartVanne = millis();
      }
      gestionVanne(1,1);
      // Simulation ouverture dure 3 secondes
      if ( (millis() - timeStartVanne) >= 3000 ){
        // réinitialise le temps de départ de l'ouverture de la vanne
        timeStartVanne = -1;
        // ferme la vanne
        gestionVanne(0,9);
        etatApplication = 0;
      }
      break;
    case etatErreur:
      Serial.println("En erreur !");
      gestionVanne(0,9);
      gestionPompe(0);
      break;
    default:
      break;
  }
}

void C2_ENVOIE_ETAT(){
  char chaine[100] = "";
  switch (etatApplication) {
    case 1:
      strcpy(chaine,"fermeture de la vanne de sortie de la cuve de stockage en cours");
      break;
    case 2:
      strcpy(chaine,"pompage du liquide vers la cuve de stockage en cours");
      break;
    case 3:
      strcpy(chaine,"ouverture de la vanne de sortie de la cuve de stockage en cours");
      break;
    case etatErreur:
      strcpy(chaine,"carte en erreur !");
      break;
    default:
      strcpy(chaine,"carte en attente, aucune action en cours");
      break;
  }
  // envoyer chaine
}

void C2_RECUPERATION_LIQUIDE_CUVE(){
  etatApplication = 1;
}

void C2_OUVRIR_VANNE() {
  //on ouvre la vanne de sortie de cuve
  etatApplication = 3;
}

void finPompe(){
  // Serial.println("fin pompe");
  // on change l'état de notre application pour passer à la phase défault
  etatApplication = 0;
  // réinitialise le temps de départ de l'ouverture de la vanne
  timeStartPompage = -1;
  // stop la pompe
  gestionPompe(0);
}

void finVanne() {
  // Serial.println("fin vanne");
  // on change l'état de notre application pour passer à la phase suivante
  etatApplication = 2;
  // réinitialise le temps de départ de l'ouverture de la vanne
  timeStartVanne = -1;
  // fin fermeture de vanne
  gestionVanne(0,9);
}

//numero 1
void gestionPompe(unsigned char state){
  if(state==1){
    digitalWrite(pompe, HIGH);
  }
  if(state==0){
    digitalWrite(pompe, LOW);
  } 
}

//numero 2
void gestionVanne(unsigned char state, unsigned char ouvrir_fermer){
  // ouvrir_fermer == 0 -> on ferme ; 1 -> on ouvre
  if(state==1){
    digitalWrite(vanne, HIGH);
    // si la vanne est ouverte on la ferme sinon on l'ouvre
    digitalWrite(enableBridge1,HIGH); // Active pont en H
    if (ouvrir_fermer == 0) {
      // Tourne dans le sens indirect
      analogWrite(MotorForward1,0);
      analogWrite(MotorReverse1,Power);
    }
    if (ouvrir_fermer == 1) {
      // Tourne dans le sens direct
      analogWrite(MotorReverse1,0);
      analogWrite(MotorForward1,Power);
    }
  }
  if(state==0){
    digitalWrite(vanne, LOW);
    // on a fini de tourner le moteur
    analogWrite(MotorForward1,0);
    analogWrite(MotorReverse1,0);
    digitalWrite(enableBridge1,LOW);
  }
}
